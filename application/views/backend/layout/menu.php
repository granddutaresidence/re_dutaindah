<ul class="sidebar-menu" data-widget="tree">
	<li class="header">MAIN NAVIGATION</li>
	<li class="treeview-menu">
	    <li><a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
	</li>
	<li class="treeview">
	  <a href="#">
	    <i class="fa fa-pie-chart"></i>
	    <span>Daftar Pengguna</span>
	    <span class="pull-right-container">
	      <i class="fa fa-angle-left pull-right"></i>
	    </span>
	  </a>
	  <ul class="treeview-menu">
	    <li><a href="<?php echo base_url(); ?>user_log"><i class="fa fa-circle-o"></i> Pengguna Masuk Dashboard</a></li>
	    <li><a href="<?php echo base_url(); ?>user_employee"><i class="fa fa-circle-o"></i> Karyawan</a></li>
	  </ul>
	</li>
	<li class="treeview">
	  <a href="#">
	    <i class="fa fa-laptop"></i>
	    <span>Daftar Anggota</span>
	    <span class="pull-right-container">
	      <i class="fa fa-angle-left pull-right"></i>
	    </span>
	  </a>
	  <ul class="treeview-menu">
	    <li><a href="<?php echo base_url(); ?>user_regis"><i class="fa fa-circle-o"></i> Anggota</a></li>
	  </ul>
	</li>
	<li class="treeview">
	  <a href="#">
	    <i class="fa fa-edit"></i> <span>Properti</span>
	    <span class="pull-right-container">
	      <i class="fa fa-angle-left pull-right"></i>
	    </span>
	  </a>
	  <ul class="treeview-menu">
	    <li><a href="<?php echo base_url(); ?>properti"><i class="fa fa-circle-o"></i> Daftar Properti</a></li>
	  </ul>
	</li>
	<li class="treeview">
	  <a href="#">
	    <i class="fa fa-table"></i> <span>KPR</span>
	    <span class="pull-right-container">
	      <i class="fa fa-angle-left pull-right"></i>
	    </span>
	  </a>
	  <ul class="treeview-menu">
	    <li><a href="pages/tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
	    <li><a href="pages/tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
	  </ul>
	</li>
	<li class="treeview">
	  <a href="#">
	    <i class="fa fa-folder"></i> <span>Konten Website</span>
	    <span class="pull-right-container">
	      <i class="fa fa-angle-left pull-right"></i>
	    </span>
	  </a>
	  <ul class="treeview-menu">
	    <li><a href="<?php echo base_url(); ?>menu"><i class="fa fa-circle-o"></i> Menu</a></li>
	    <li><a href="<?php echo base_url(); ?>sub_menu"><i class="fa fa-circle-o"></i> Sub Menu</a></li>
	    <li><a href="<?php echo base_url(); ?>konten"><i class="fa fa-circle-o"></i> Konten</a></li>
	    <li><a href="<?php echo base_url(); ?>sub_konten"><i class="fa fa-circle-o"></i> Sub Konten/a></li>
	    <li><a href="pages/examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
	    <li><a href="pages/examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
	    <li><a href="pages/examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
	    <li><a href="pages/examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
	    <li><a href="pages/examples/pace.html"><i class="fa fa-circle-o"></i> Pace Page</a></li>
	  </ul>
	</li>
	<li class="treeview">
	  <a href="#">
	    <i class="fa fa-share"></i> <span>Penawaran</span>
	    <span class="pull-right-container">
	      <i class="fa fa-angle-left pull-right"></i>
	    </span>
	  </a>
	  <ul class="treeview-menu">
	    <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
	    <li class="treeview">
	      <a href="#"><i class="fa fa-circle-o"></i> Level One
	        <span class="pull-right-container">
	          <i class="fa fa-angle-left pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">
	        <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
	        <li class="treeview">
	          <a href="#"><i class="fa fa-circle-o"></i> Level Two
	            <span class="pull-right-container">
	              <i class="fa fa-angle-left pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
	            <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
	          </ul>
	        </li>
	      </ul>
	    </li>
	    <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
	  </ul>
	</li>
</ul>
<script>
$(function(){

    var url = "#", // in real app this would have to be replaced with window.location.pathname 
        urlRegExp = new RegExp(url.replace(/\/$/,'')); // create regexp to match current url pathname and remove trailing slash if present as it could collide with the link in navigation in case trailing slash wasn't present there
    
        // now grab every link from the navigation
        $('.sidebar-menu a').each(function(){
            // and test its href against the url pathname regexp
            if(urlRegExp.test(this.href)){
                $(this).addClass('active');
            }
        });

});
</script>