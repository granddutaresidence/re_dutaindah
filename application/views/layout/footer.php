
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-submenu.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/rangeslider.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.mb.YTPlayer.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.scrollUp.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/leaflet.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/leaflet-providers.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/leaflet.markercluster.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/dropzone.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.filterizr.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/maps.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ie10-viewport-bug-workaround.js"></script>
<!-- Custom javascript -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ie10-viewport-bug-workaround.js"></script>