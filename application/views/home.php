<?php
    $this->load->view('layout/head');
?>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="<?php echo base_url(); ?>assets/<?php echo base_url(); ?>assets/https://www.googletagmanager.com/ns.html?id=GTM-P5MJCCG"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="page_loader"></div>

<!-- Top header start -->
<header class="top-header hidden-xs" id="top">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="list-inline">
                    <a href="#"><i class="fa fa-phone"></i>(021) 557.30.888</a>
                    <a href="grandduta123@gmail.com"><i class="fa fa-envelope"></i>grandduta123@gmail.com</a>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Top header end -->

<!-- Main header start -->
<header class="main-header">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navigation" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="index.html" class="logo">
                    <img src="<?php echo base_url(); ?>assets/<?php echo base_url(); ?>assets/img/logos/logo.png" alt="logo">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-collapse collapse" role="navigation" aria-expanded="true" id="app-navigation">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                            Beranda<!-- <span class="caret"></span> -->
                        </a>
                        <!-- <ul class="dropdown-menu">
                            <li><a href="index.html">Home 1</a></li>
                            <li><a href="index-2.html">Home 2</a></li>
                            <li><a href="index-3.html">Home 3</a></li>
                            <li><a href="index-4.html">Home 4</a></li>
                            <li><a href="index-5.html">Home 5</a></li>
                        </ul> -->
                    </li>
                    <li class="">
                        <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                            Dijual<!-- <span class="caret"></span> -->
                        </a>
                        <!-- <ul class="dropdown-menu">
                            <li><a href="index.html">Home 1</a></li>
                            <li><a href="index-2.html">Home 2</a></li>
                            <li><a href="index-3.html">Home 3</a></li>
                            <li><a href="index-4.html">Home 4</a></li>
                            <li><a href="index-5.html">Home 5</a></li>
                        </ul> -->
                    </li>
                    <li class="">
                        <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                            Disewa<!-- <span class="caret"></span> -->
                        </a>
                        <!-- <ul class="dropdown-menu">
                            <li><a href="index.html">Home 1</a></li>
                            <li><a href="index-2.html">Home 2</a></li>
                            <li><a href="index-3.html">Home 3</a></li>
                            <li><a href="index-4.html">Home 4</a></li>
                            <li><a href="index-5.html">Home 5</a></li>
                        </ul> -->
                    </li>
                    <li class="">
                        <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                            Properti Baru<!-- <span class="caret"></span> -->
                        </a>
                        <!-- <ul class="dropdown-menu">
                            <li><a href="index.html">Home 1</a></li>
                            <li><a href="index-2.html">Home 2</a></li>
                            <li><a href="index-3.html">Home 3</a></li>
                            <li><a href="index-4.html">Home 4</a></li>
                            <li><a href="index-5.html">Home 5</a></li>
                        </ul> -->
                    </li>
                    <li class="">
                        <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                            KPR<!-- <span class="caret"></span> -->
                        </a>
                        <!-- <ul class="dropdown-menu">
                            <li><a href="index.html">Home 1</a></li>
                            <li><a href="index-2.html">Home 2</a></li>
                            <li><a href="index-3.html">Home 3</a></li>
                            <li><a href="index-4.html">Home 4</a></li>
                            <li><a href="index-5.html">Home 5</a></li>
                        </ul> -->
                    </li>
                    <li class="">
                        <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                            Hubungi Kami<!-- <span class="caret"></span> -->
                        </a>
                        <!-- <ul class="dropdown-menu">
                            <li><a href="index.html">Home 1</a></li>
                            <li><a href="index-2.html">Home 2</a></li>
                            <li><a href="index-3.html">Home 3</a></li>
                            <li><a href="index-4.html">Home 4</a></li>
                            <li><a href="index-5.html">Home 5</a></li>
                        </ul> -->
                    </li>
                    <li class="">
                        <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                            Tentang Kami<!-- <span class="caret"></span> -->
                        </a>
                        <!-- <ul class="dropdown-menu">
                            <li><a href="index.html">Home 1</a></li>
                            <li><a href="index-2.html">Home 2</a></li>
                            <li><a href="index-3.html">Home 3</a></li>
                            <li><a href="index-4.html">Home 4</a></li>
                            <li><a href="index-5.html">Home 5</a></li>
                        </ul> -->
                    </li>
                    <!-- <li class="dropdown">
                        <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                            Properties<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-submenu">
                                <a tabindex="0">Properties List</a>
                                <ul class="dropdown-menu">
                                    <li><a href="properties-list-rightside.html">Properties List Right Sidebar</a></li>
                                    <li><a href="properties-list-leftside.html">Properties List Left Sidebar</a></li>
                                    <li><a href="properties-list-fullwidth.html">Properties Full Width</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu">
                                <a tabindex="0">Properties Grid</a>
                                <ul class="dropdown-menu">
                                    <li><a href="properties-grid-rightside.html">Properties Grid Right Sidebar</a></li>
                                    <li><a href="properties-grid-leftside.html">Properties Grid Left Sidebar</a></li>
                                    <li><a href="properties-grid-fullwidth.html">Properties Grid Full Width</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu">
                                <a tabindex="0">Map View</a>
                                <ul class="dropdown-menu">
                                    <li><a href="properties-map-leftside-list.html">Map Left Sidebar List</a></li>
                                    <li><a href="properties-map-rightside-list.html">Map Right Sidebar List</a></li>
                                    <li><a href="properties-map-leftside-grid.html">Map Left Sidebar Grid</a></li>
                                    <li><a href="properties-map-rightside-grid.html">Map Right Sidebar Grid</a></li>
                                    <li><a href="properties-map-full.html">Map Full Width</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu">
                                <a tabindex="0">Property Detail</a>
                                <ul class="dropdown-menu">
                                    <li><a href="properties-details.html">Property Detail 1</a></li>
                                    <li><a href="properties-details-2.html">Property Detail 2</a></li>
                                    <li><a href="properties-details-3.html">Property Detail 3</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li> -->
                    <!-- <li class="dropdown">
                        <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                            Agents<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="agent-listing-grid.html">Agent grid</a></li>
                            <li><a href="agent-listing-grid-sidebar.html">Agent Grid sidebarbar</a></li>
                            <li><a href="agent-listing-row.html">Agent list</a></li>
                            <li><a href="agent-listing-row-sidebar.html">Agent List Sidebarbar</a></li>
                            <li><a href="agent-single.html">Agent Detail</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                            Blog<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="blog-right-sidebar.html">Blog right sidebar</a></li>
                            <li><a href="blog-left-sidebar.html">Blog left sidebar</a></li>
                            <li><a href="blog-full-width.html">Blog full width</a></li>
                            <li><a href="blog-creative.html">Blog creative</a></li>
                            <li><a href="blog-single.html">Blog Detail</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                            Pages<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-submenu">
                                <a tabindex="0">Extras</a>
                                <ul class="dropdown-menu">
                                    <li><a href="typography.html">Typography</a></li>
                                    <li><a href="pricing-tables.html">Pricing Tables</a></li>
                                    <li><a href="elements.html">Elements</a></li>
                                    <li><a href="icon.html">icon</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu">
                                <a tabindex="0">User Profile</a>
                                <ul class="dropdown-menu">
                                    <li><a href="user-profile.html">User profile</a></li>
                                    <li><a href="my-properties.html">My Properties</a></li>
                                    <li><a href="favorited-properties.html">Favorited Properties</a></li>
                                    <li><a href="submit-property.html">Submit Property</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu">
                                <a tabindex="0">Contact</a>
                                <ul class="dropdown-menu">
                                    <li><a href="contact.html">Contact</a></li>
                                    <li><a href="contact-2.html">Contact 2</a></li>
                                </ul>
                            </li>
                            <li><a href="about.html">About Us</a></li>
                            <li><a href="faq.html">Faq</a></li>
                            <li><a href="login.html">Login</a></li>
                            <li><a href="signup.html">Signup</a></li>
                            <li><a href="forgot-password.html">Forgot Password</a></li>
                            <li><a href="404.html">404 Error</a></li>
                        </ul>
                    </li> -->
                </ul>
                <!-- <ul class="nav navbar-nav navbar-right rightside-navbar">
                    <li>
                        <a href="submit-property.html" class="button">
                            Submit Property
                        </a>
                    </li>
                </ul> -->
            </div>

            <!-- /.navbar-collapse -->
            <!-- /.container -->
        </nav>
    </div>
</header>
<!-- Main header end -->

<!-- Banner start -->
<div class="banner">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="<?php echo base_url(); ?>assets/img/banner/1.jpg" alt="banner-slider-1">
                <div class="carousel-caption banner-slider-inner banner-top-align">
                    <div class="text-right">
                    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                        <h2 data-animation="animated fadeInDown delay-05s"><span>ALEXANDRITE 60</span> <br>Dijual</h2>
                        <!-- <a href="#" class="btn button-md button-theme" data-animation="animated fadeInUp delay-05s">Get Started Now</a> -->
                        <!-- <a href="#" class="btn button-md border-button-theme" data-animation="animated fadeInUp delay-05s">Learn More</a> -->
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="<?php echo base_url(); ?>assets/img/banner/2.jpg" alt="banner-slider-2">
                <div class="carousel-caption banner-slider-inner banner-top-align">
                    <div class="text-right">
                    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                        <h2 data-animation="animated fadeInDown delay-05s"><span>ALEXANDRITE 90</span> <br>Dijual</h2>
                        <!-- <a href="#" class="btn button-md button-theme" data-animation="animated fadeInUp delay-05s">Get Started Now</a> -->
                        <!-- <a href="#" class="btn button-md border-button-theme" data-animation="animated fadeInUp delay-05s">Learn More</a> -->
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="<?php echo base_url(); ?>assets/img/banner/3.jpg" alt="banner-slider-3">
                <div class="carousel-caption banner-slider-inner banner-top-align">
                    <div class="text-right">
                    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                        <h2 data-animation="animated fadeInDown delay-05s"><span>RUKAN EMERALD ARCADE</span> <br>Dijual</h2>
                        <!-- <a href="#" class="btn button-md button-theme" data-animation="animated fadeInUp delay-05s">Get Started Now</a> -->
                        <!-- <a href="#" class="btn button-md border-button-theme" data-animation="animated fadeInUp delay-05s">Learn More</a> -->
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="<?php echo base_url(); ?>assets/img/banner/4.jpg" alt="banner-slider-3">
                <div class="carousel-caption banner-slider-inner banner-top-align">
                    <div class="text-right">
                    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                        <h2 data-animation="animated fadeInDown delay-05s"><span>Alexandrite 105</span> <br>Dijual</h2>
                        <!-- <a href="#" class="btn button-md button-theme" data-animation="animated fadeInUp delay-05s">Get Started Now</a> -->
                        <!-- <a href="#" class="btn button-md border-button-theme" data-animation="animated fadeInUp delay-05s">Learn More</a> -->
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="<?php echo base_url(); ?>assets/img/banner/5.jpg" alt="banner-slider-3">
                <div class="carousel-caption banner-slider-inner banner-top-align">
                    <div class="text-right">
                    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                        <h2 data-animation="animated fadeInDown delay-05s"><span>Garnet</span> <br>Dijual</h2>
                        <!-- <a href="#" class="btn button-md button-theme" data-animation="animated fadeInUp delay-05s">Get Started Now</a> -->
                        <!-- <a href="#" class="btn button-md border-button-theme" data-animation="animated fadeInUp delay-05s">Learn More</a> -->
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="<?php echo base_url(); ?>assets/img/banner/6.jpg" alt="banner-slider-3">
                <div class="carousel-caption banner-slider-inner banner-top-align">
                    <div class="text-right">
                    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                        <h2 data-animation="animated fadeInDown delay-05s"><span>Emerald 48</span> <br>Dijual</h2>
                        <!-- <a href="#" class="btn button-md button-theme" data-animation="animated fadeInUp delay-05s">Get Started Now</a> -->
                        <!-- <a href="#" class="btn button-md border-button-theme" data-animation="animated fadeInUp delay-05s">Learn More</a> -->
                    </div>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="slider-mover-left" aria-hidden="true">
                <i class="fa fa-angle-left"></i>
            </span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="slider-mover-right" aria-hidden="true">
                <i class="fa fa-angle-right"></i>
            </span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<!-- Banner end -->

<!-- Search area start -->
<!-- <div class="search-area">
    <div class="container">
        <div class="search-area-inner">
            <div class="search-contents ">
                <form method="GET">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <select class="selectpicker search-fields" name="area-from" data-live-search="true" data-live-search-placeholder="Search value">
                                    <option>Area From</option>
                                    <option>1000</option>
                                    <option>800</option>
                                    <option>600</option>
                                    <option>400</option>
                                    <option>200</option>
                                    <option>100</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <select class="selectpicker search-fields" name="property-status" data-live-search="true" data-live-search-placeholder="Search value">
                                    <option>Property Status</option>
                                    <option>For Sale</option>
                                    <option>For Rent</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <select class="selectpicker search-fields" name="location" data-live-search="true" data-live-search-placeholder="Search value">
                                    <option>Location</option>
                                    <option>United States</option>
                                    <option>United Kingdom</option>
                                    <option>American Samoa</option>
                                    <option>Belgium</option>
                                    <option>Cameroon</option>
                                    <option>Canada</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <select class="selectpicker search-fields" name="property-types" data-live-search="true" data-live-search-placeholder="Search value">
                                    <option>Property Types</option>
                                    <option>Residential</option>
                                    <option>Commercial</option>
                                    <option>Land</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <select class="selectpicker search-fields" name="bedrooms" data-live-search="true" data-live-search-placeholder="Search value" >
                                    <option>Bedrooms</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                    <option>7</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <select class="selectpicker search-fields" name="bathrooms" data-live-search="true" data-live-search-placeholder="Search value" >
                                    <option>Bathrooms</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                    <option>7</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <div class="range-slider">
                                    <div data-min="0" data-max="150000" data-unit="USD" data-min-name="min_price" data-max-name="max_price" class="range-slider-ui ui-slider" aria-disabled="false"></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 ">
                            <div class="form-group">
                                <button class="search-button">Search</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> -->
<!-- Search area start -->

<!-- Featured properties start -->
<div class="content-area featured-properties">
    <div class="container">
        <!-- Main title -->
        <div class="main-title">
            <h1><span>Rumah</span> Dijual</h1>
        </div>
        <ul class="list-inline-listing filters filters-listing-navigation">
            <li class="active btn filtr-button filtr" data-filter="1">Rumah</li>
            <!-- <li data-filter="1" class="btn btn-inline filtr-button filtr">House</li>
            <li data-filter="2" class="btn btn-inline filtr-button filtr">Office</li>
            <li data-filter="3" class="btn btn-inline filtr-button filtr">Apartment</li>
            <li data-filter="4" class="btn btn-inline filtr-button filtr">Residential</li> -->
        </ul>
        <div class="row">
            <div class="filtr-container">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12  filtr-item" data-category="1">
                    <div class="property">
                        <!-- Property img -->
                        <a href="#" class="property-img">
                            <div class="property-tag button alt featured">AX 60</div>
                            <div class="property-tag button sale">Dijual</div>
                            <div class="property-price">Rp. 920.764.000</div>
                            <img src="<?php echo base_url(); ?>assets/img/properties/ax60.jpg" alt="properties-1" class="img-responsive">
                        </a>
                        <!-- Property content -->
                        <div class="property-content">
                            <!-- title -->
                            <h1 class="title">
                                <a href="#">Alexandrite 60</a>
                            </h1>
                            <!-- Property address -->
                            <!-- <h3 class="property-address">
                                <a href="properties-details.html">
                                    <i class="fa fa-map-marker"></i>123 Kathal St. Tampa City,
                                </a>
                            </h3> -->
                            <!-- Facilities List -->
                            <ul class="facilities-list clearfix">
                                <li>
                                    <i class="flaticon-square-layouting-with-black-square-in-east-area"></i>
                                    <span>LB : 48 LT : 60</span>
                                </li>
                                <li>
                                    <i class="flaticon-bed"></i>
                                    <span>2 K. Tidur</span>
                                </li>
                                <li>
                                    <i class="flaticon-monitor"></i>
                                    <span>1 Ruang TV </span>
                                </li>
                                <li>
                                    <i class="flaticon-holidays"></i>
                                    <span>2 K. Mandi</span>
                                </li>
                                <li>
                                    <i class="flaticon-vehicle"></i>
                                    <span>1 Garasi</span>
                                </li>
                                <li>
                                    <i class="flaticon-building"></i>
                                    <span>1 Balkon atas</span>
                                </li>
                                <li>
                                    <i class="flaticon-technology"></i>
                                    <span>1 Karpot</span>
                                </li>
                                <li>
                                    <i class="flaticon-park"></i>
                                    <span>1 Taman</span>
                                </li>
                            </ul>
                            <!-- Property footer -->
                            <!-- <div class="property-footer">
                                <span class="left"><i class="fa fa-calendar-o icon"></i> 5 days ago</span>
                                <span class="right">
                                                    <a href="#"><i class="fa fa-heart-o icon"></i></a>
                                                    <a href="#"><i class="fa fa-share-alt"></i></a>
                                                </span>
                            </div> -->
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12  filtr-item" data-category="1">
                    <div class="property">
                        <!-- Property img -->
                        <a href="#" class="property-img">
                            <div class="property-tag button alt featured">AX 90</div>
                            <div class="property-tag button sale">Dijual</div>
                            <div class="property-price">Rp. 1.183.290.100</div>
                            <img src="<?php echo base_url(); ?>assets/img/properties/ax90.jpg" alt="properties-3" class="img-responsive">
                        </a>
                        <!-- Property content -->
                        <div class="property-content">
                            <!-- title -->
                            <h1 class="title">
                                <a href="#">Alexandrite 90</a>
                            </h1>
                            <!-- Property address -->
                            <!-- <h3 class="property-address">
                                <a href="properties-details.html">
                                    <i class="fa fa-map-marker"></i>123 Kathal St. Tampa City,
                                </a>
                            </h3> -->
                            <!-- Facilities List -->
                            <ul class="facilities-list clearfix">
                                <li>
                                    <i class="flaticon-square-layouting-with-black-square-in-east-area"></i>
                                    <span>LB : 65 LT : 90</span>
                                </li>
                                <li>
                                    <i class="flaticon-bed"></i>
                                    <span>4 K. Tidur</span>
                                </li>
                                <li>
                                    <i class="flaticon-monitor"></i>
                                    <span>1 Ruang TV </span>
                                </li>
                                <li>
                                    <i class="flaticon-holidays"></i>
                                    <span>3 K. Mandi</span>
                                </li>
                                <li>
                                    <i class="flaticon-vehicle"></i>
                                    <span>1 Garasi</span>
                                </li>
                                <li>
                                    <i class="flaticon-building"></i>
                                    <span>1 Balkon atas</span>
                                </li>
                                <li>
                                    <i class="flaticon-technology"></i>
                                    <span>1 Karpot</span>
                                </li>
                                <li>
                                    <i class="flaticon-park"></i>
                                    <span>1 Taman</span>
                                </li>
                            </ul>
                            <!-- Property footer -->
                            <!-- <div class="property-footer">
                                <span class="left"><i class="fa fa-calendar-o icon"></i> 5 days ago</span>
                                <span class="right">
                                                    <a href="#"><i class="fa fa-heart-o icon"></i></a>
                                                    <a href="#"><i class="fa fa-share-alt"></i></a>
                                                </span>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12  filtr-item" data-category="1">
                    <div class="property">
                        <!-- Property img -->
                        <a href="#" class="property-img">
                            <div class="property-tag button alt featured">Rukan Emerald Arcade</div>
                            <div class="property-tag button sale">Dijual</div>
                            <div class="property-price">Rp. 1.659.703</div>
                            <img src="<?php echo base_url(); ?>assets/img/properties/rukan.jpg" alt="properties-5" class="img-responsive">
                        </a>
                        <!-- Property content -->
                        <div class="property-content">
                            <!-- title -->
                            <h1 class="title">
                                <a href="#">Rukan Emerald Arcade</a>
                            </h1>
                            <!-- Property address -->
                            <!-- <h3 class="property-address">
                                <a href="properties-details.html">
                                    <i class="fa fa-map-marker"></i>123 Kathal St. Tampa City,
                                </a>
                            </h3> -->
                            <!-- Facilities List -->
                            <ul class="facilities-list clearfix">
                                <li>
                                    <i class="flaticon-square-layouting-with-black-square-in-east-area"></i>
                                    <span>4.5 x 16 m2</span>
                                </li>
                                <li>
                                    <i class="flaticon-bed"></i>
                                    <span>2 K. Tidur</span>
                                </li>
                                <li>
                                    <i class="flaticon-monitor"></i>
                                    <span>1 R. Utama </span>
                                </li>
                                <li>
                                    <i class="flaticon-holidays"></i>
                                    <span>1 K. Mandi</span>
                                </li>
                                <li>
                                    <i class="flaticon-vehicle"></i>
                                    <span>1 Garasi</span>
                                </li>
                                <li>
                                    <i class="flaticon-building"></i>
                                    <span>1 Balkon atas</span>
                                </li>
                                <li>
                                    <i class="flaticon-technology"></i>
                                    <span>1 Karpot</span>
                                </li>
                                <li>
                                    <i class="flaticon-park"></i>
                                    <span>1 Taman</span>
                                </li>
                            </ul>
                            <!-- Property footer -->
                            <!-- <div class="property-footer">
                                <span class="left"><i class="fa fa-calendar-o icon"></i> 5 days ago</span>
                                <span class="right">
                                                    <a href="#"><i class="fa fa-heart-o icon"></i></a>
                                                    <a href="#"><i class="fa fa-share-alt"></i></a>
                                                </span>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12  filtr-item" data-category="1">
                    <div class="property">
                        <!-- Property img -->
                        <a href="#" class="property-img">
                            <div class="property-tag button alt featured">Garnet</div>
                            <div class="property-tag button sale">Dijual</div>
                            <div class="property-price">Rp. 850.862.000</div>
                            <img src="<?php echo base_url(); ?>assets/img/properties/garnet.jpg" alt="properties-5" class="img-responsive">
                        </a>
                        <!-- Property content -->
                        <div class="property-content">
                            <!-- title -->
                            <h1 class="title">
                                <a href="#">Garnet</a>
                            </h1>
                            <!-- Property address -->
                            <!-- <h3 class="property-address">
                                <a href="properties-details.html">
                                    <i class="fa fa-map-marker"></i>123 Kathal St. Tampa City,
                                </a>
                            </h3> -->
                            <!-- Facilities List -->
                            <ul class="facilities-list clearfix">
                                <li>
                                    <i class="flaticon-square-layouting-with-black-square-in-east-area"></i>
                                    <span>LB : 36 LT : 72</span>
                                </li>
                                <li>
                                    <i class="flaticon-bed"></i>
                                    <span>2 K. Tidur</span>
                                </li>
                                <li>
                                    <i class="flaticon-monitor"></i>
                                    <span>1 R. Tamu </span>
                                </li>
                                <li>
                                    <i class="flaticon-holidays"></i>
                                    <span>1 K. Mandi</span>
                                </li>
                                <li>
                                    <i class="flaticon-vehicle"></i>
                                    <span>1 Garasi</span>
                                </li>
                                <li>
                                    <i class="flaticon-building"></i>
                                    <span>1 Balkon atas</span>
                                </li>
                                <li>
                                    <i class="flaticon-technology"></i>
                                    <span>1 Karpot</span>
                                </li>
                                <li>
                                    <i class="flaticon-park"></i>
                                    <span>1 Taman</span>
                                </li>
                            </ul>
                            <!-- Property footer -->
                            <!-- <div class="property-footer">
                                <span class="left"><i class="fa fa-calendar-o icon"></i> 5 days ago</span>
                                <span class="right">
                                                    <a href="#"><i class="fa fa-heart-o icon"></i></a>
                                                    <a href="#"><i class="fa fa-share-alt"></i></a>
                                                </span>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12  filtr-item" data-category="1">
                    <div class="property">
                        <!-- Property img -->
                        <a href="#" class="property-img">
                            <div class="property-tag button alt featured">Emerald 48</div>
                            <div class="property-tag button sale">Dijual</div>
                            <div class="property-price">Rp. 862.762.856</div>
                            <img src="<?php echo base_url(); ?>assets/img/properties/emerald.jpg" alt="properties-5" class="img-responsive">
                        </a>
                        <!-- Property content -->
                        <div class="property-content">
                            <!-- title -->
                            <h1 class="title">
                                <a href="#">Emerald 48</a>
                            </h1>
                            <!-- Property address -->
                            <!-- <h3 class="property-address">
                                <a href="properties-details.html">
                                    <i class="fa fa-map-marker"></i>123 Kathal St. Tampa City,
                                </a>
                            </h3> -->
                            <!-- Facilities List -->
                            <ul class="facilities-list clearfix">
                                <li>
                                    <i class="flaticon-square-layouting-with-black-square-in-east-area"></i>
                                    <span>LB : 36 LT : 87</span>
                                </li>
                                <li>
                                    <i class="flaticon-bed"></i>
                                    <span>2 K. Tidur</span>
                                </li>
                                <li>
                                    <i class="flaticon-monitor"></i>
                                    <span>1 R. Tamu </span>
                                </li>
                                <li>
                                    <i class="flaticon-holidays"></i>
                                    <span>1 K. Mandi</span>
                                </li>
                                <li>
                                    <i class="flaticon-vehicle"></i>
                                    <span>1 Garasi</span>
                                </li>
                                <li>
                                    <i class="flaticon-building"></i>
                                    <span>1 Balkon atas</span>
                                </li>
                                <li>
                                    <i class="flaticon-technology"></i>
                                    <span>1 Karpot</span>
                                </li>
                                <li>
                                    <i class="flaticon-park"></i>
                                    <span>1 Taman</span>
                                </li>
                            </ul>
                            <!-- Property footer -->
                            <!-- <div class="property-footer">
                                <span class="left"><i class="fa fa-calendar-o icon"></i> 5 days ago</span>
                                <span class="right">
                                                    <a href="#"><i class="fa fa-heart-o icon"></i></a>
                                                    <a href="#"><i class="fa fa-share-alt"></i></a>
                                                </span>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Featured properties end -->

<!-- Our service start -->
<div class="mrg-btm-100 our-service">
    <div class="container">
        <!-- Main title -->
        <div class="main-title">
            <h1><span>Fasilitas</span> umum</h1>
        </div>

        <div class="row mgn-btm wow">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 wow fadeInLeft delay-04s">
                <div class="content">
                    <i class="flaticon-apartment"></i>
                    <h4>Gedung Serba Guna</h4>
                    <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p> -->
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 wow fadeInLeft delay-04s">
                <div class="content">
                    <i class="flaticon-internet"></i>
                    <h4>Sport Club</h4>
                    <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p> -->
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 wow fadeInRight delay-04s">
                <div class="content">
                    <i class="flaticon-vehicle"></i>
                    <h4>Akses Jalan Tembus</h4>
                    <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p> -->
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 wow fadeInRight delay-04s">
                <div class="content">
                    <i class="flaticon-symbol"></i>
                    <h4>FoodCourt</h4>
                    <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p> -->
                </div>
            </div>
        </div>
        <a href="#" class="btn button-md button-theme">Lihat Lainnya</a>
    </div>
</div>
<!-- Our service end -->

<!-- Recently properties start -->

<!-- Partners block end -->

<div class="clearfix"></div>
<!-- Categories strat -->

<!-- Categories end-->

<!-- Agent section start -->
<div class="mrg-btm-70 agent-section chevron-icon">
    <div class="container">
        <!-- Main title -->
        <div class="main-title">
            <h1><span>Sales</span> Agent</h1>
        </div>
        <div class="row">
            <div class="carousel our-partners" id="">
                <!-- <div class="col-lg-12 mrg-btm-30">
                    <a class="right carousel-control" href="#ourPartners3" data-slide="prev"><i class="fa fa-chevron-left icon-prev"></i></a>
                    <a class="right carousel-control" href="#ourPartners3" data-slide="next"><i class="fa fa-chevron-right icon-next"></i></a>
                </div> -->
                <div class="">
                <div class="item active">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <!-- Agent box start -->
                            <div class="agent-box">
                                <!-- Agent img -->
                                <a href="#" class="agent-img">
                                    <img src="<?php echo base_url(); ?>assets/img/team/team1.jpg" class="img-responsive">
                                </a>
                                <!-- Agent content -->
                                <div class="agent-content">
                                    <!-- title -->
                                    <h1 class="title">
                                        <a href="agent-single.html">David</a>
                                    </h1>
                                    <!-- Contact -->
                                    <div class="contact">
                                        <p>
                                            <a href="mailto:info@themevessel.com"><i class="fa fa-envelope-o"></i>d_vidz87@yahoo.co.id</a>
                                        </p>
                                        <p>
                                            <a href="tel:+554XX-634-7071"><i class="fa fa-whatsapp"></i>0821 2229 0057</a>
                                        </p>
                                        <p>
                                            <a href="tel:+554XX-634-7071"><i class="fa fa-phone"></i>0877 8282 7257</a>
                                        </p>
                                    </div>
                                    <!-- Social list -->
                                    <!-- <ul class="social-list clearfix">
                                        <li>
                                            <a href="#" class="facebook">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="twitter">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="linkedin">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="google">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="rss">
                                                <i class="fa fa-rss"></i>
                                            </a>
                                        </li>
                                    </ul> -->
                                </div>
                            </div>
                            <!-- Agent box end -->
                        </div>
                    </div>
                    <div class="item active">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <!-- Agent box start -->
                            <div class="agent-box">
                                <!-- Agent img -->
                                <a href="#" class="agent-img">
                                    <img src="<?php echo base_url(); ?>assets/img/team/team5.jpg" class="img-responsive">
                                </a>
                                <!-- Agent content -->
                                <div class="agent-content">
                                    <!-- title -->
                                    <h1 class="title">
                                        <a href="agent-single.html">Lydia</a>
                                    </h1>
                                    <!-- Contact -->
                                    <div class="contact">
                                        <p>
                                            <a href="mailto:info@themevessel.com"><i class="fa fa-envelope-o"></i>lidia.paloren@gmail.com</a>
                                        </p>
                                        <p>
                                            <a href="tel:+554XX-634-7071"><i class="fa fa-whatsapp"></i>0817 0879 207</a>
                                        </p>
                                        <p>
                                            <a href="tel:+554XX-634-7071"><i class="fa fa-phone"></i>0813 9997 3993</a>
                                        </p>
                                        
                                    </div>
                                    <!-- Social list -->
                                    <!-- <ul class="social-list clearfix">
                                        <li>
                                            <a href="#" class="facebook">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="twitter">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="linkedin">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="google">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="rss">
                                                <i class="fa fa-rss"></i>
                                            </a>
                                        </li>
                                    </ul> -->
                                </div>
                            </div>
                            <!-- Agent box end -->
                        </div>
                    </div>
                    <div class="item active">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <!-- Agent box start -->
                            <div class="agent-box">
                                <!-- Agent img -->
                                <a href="#" class="agent-img">
                                    <img src="<?php echo base_url(); ?>assets/img/team/team2.jpg" class="img-responsive">
                                </a>
                                <!-- Agent content -->
                                <div class="agent-content">
                                    <!-- title -->
                                    <h1 class="title">
                                        <a href="agent-single.html">Irvan Sebastian</a>
                                    </h1>
                                    <!-- Contact -->
                                    <div class="contact">
                                        <p>
                                            <a href="mailto:info@themevessel.com"><i class="fa fa-envelope-o"></i>sebastian.airoshi@gmail.com</a>
                                        </p>
                                        <p>
                                            <a href="tel:+554XX-634-7071"><i class="fa fa-phone"></i>0858 1159 9577</a>
                                        </p>
                                        <p>
                                            <a href="tel:+554XX-634-7071"><i class="fa fa-phone"></i>0812 9656 0393</a>
                                        </p>
                                    </div>
                                    <!-- Social list -->
                                    <!-- <ul class="social-list clearfix">
                                        <li>
                                            <a href="#" class="facebook">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="twitter">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="linkedin">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="google">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="rss">
                                                <i class="fa fa-rss"></i>
                                            </a>
                                        </li>
                                    </ul> -->
                                </div>
                            </div>
                            <!-- Agent box end -->
                        </div>
                    </div>
                    <div class="item active">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <!-- Agent box start -->
                            <div class="agent-box">
                                <!-- Agent img -->
                                <a href="#" class="agent-img">
                                    <img src="<?php echo base_url(); ?>assets/img/team/team3.jpg" class="img-responsive">
                                </a>
                                <!-- Agent content -->
                                <div class="agent-content">
                                    <!-- title -->
                                    <h1 class="title">
                                        <a href="agent-single.html">Alief</a>
                                    </h1>
                                    <!-- Contact -->
                                    <div class="contact">
                                        <p>
                                            <a href="mailto:info@themevessel.com"><i class="fa fa-envelope-o"></i>abdulalief84@gmail.com</a>
                                        </p>
                                        <p>
                                            <a href="tel:+554XX-634-7071"><i class="fa fa-whatsapp"></i>0812 1373 6917</a>
                                        </p>
                                        <p>
                                            <a href="tel:+554XX-634-7071"><i class="fa fa-phone"></i>0877 8112 5520</a>
                                        </p>
                                    </div>
                                    <!-- Social list -->
                                    <!-- <ul class="social-list clearfix">
                                        <li>
                                            <a href="#" class="facebook">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="twitter">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="linkedin">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="google">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="rss">
                                                <i class="fa fa-rss"></i>
                                            </a>
                                        </li>
                                    </ul> -->
                                </div>
                            </div>
                            <!-- Agent box end -->
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <!-- Agent box end -->
                            <div class="agent-box">
                                <!-- Agent img -->
                                <a href="#" class="agent-img">
                                    <img src="<?php echo base_url(); ?>assets/img/team/team7.jpg" class="img-responsive">
                                </a>
                                <!-- Agent content -->
                                <div class="agent-content">
                                    <!-- title -->
                                    <h1 class="title">
                                        <a href="agent-single.html">Shella Anggraini</a>
                                    </h1>
                                    <!-- Contact -->
                                    <div class="contact">
                                        <p>
                                            <a href="mailto:info@themevessel.com"><i class="fa fa-envelope-o"></i>felisyaanggraini97@gmail.com</a>
                                        </p>
                                        <p>
                                            <a href="tel:+554XX-634-7071"><i class="fa fa-whatsapp"></i>0821 1262 2670</a>
                                        </p>
                                        <p>
                                            <a href="tel:+554XX-634-7071"><i class="fa fa-phone"></i>0899 1857 762</a>
                                        </p>
                                    </div>
                                    <!-- Sociallist -->
                                    <!-- <ul class="social-list clearfix">
                                        <li>
                                            <a href="#" class="facebook">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="twitter">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="linkedin">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="google">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="rss">
                                                <i class="fa fa-rss"></i>
                                            </a>
                                        </li>
                                    </ul> -->
                                </div>
                            </div>
                            <!-- Agent box end -->
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <!-- Agent box start -->
                            <div class="agent-box">
                                <!-- Agent img -->
                                <a href="#" class="agent-img">
                                    <img src="<?php echo base_url(); ?>assets/img/team/team8.jpg" class="img-responsive">
                                </a>
                                <!-- Agent content -->
                                <div class="agent-content">
                                    <!-- title -->
                                    <h1 class="title">
                                        <a href="agent-single.html">Runie</a>
                                    </h1>
                                    <!-- Contact -->
                                    <div class="contact">
                                        <p>
                                            <a href="mailto:info@themevessel.com"><i class="fa fa-envelope-o"></i>runie90.m@gmail.com</a>
                                        </p>
                                        <p>
                                            <a href="tel:+554XX-634-7071"><i class="fa fa-whatsapp"></i>0812 99733 0866</a>
                                        </p>
                                        <p>
                                            <a href="tel:+554XX-634-7071"><i class="fa fa-phone"></i>0856 9725 4866</a>
                                        </p>
                                    </div>
                                    <!-- Sociallist -->
                                    <!-- <ul class="social-list clearfix">
                                        <li>
                                            <a href="#" class="facebook">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="twitter">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="linkedin">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="google">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="rss">
                                                <i class="fa fa-rss"></i>
                                            </a>
                                        </li>
                                    </ul> -->
                                </div>
                            </div>
                            <!-- Agent box enx -->
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <!-- Agent bos start -->
                            <div class="agent-box">
                                <!-- Agent img -->
                                <a href="#" class="agent-img">
                                    <img src="<?php echo base_url(); ?>assets/img/team/team6.jpg" class="img-responsive">
                                </a>
                                <!-- Agent content -->
                                <div class="agent-content">
                                    <!-- title -->
                                    <h1 class="title">
                                        <a href="agent-single.html">Aya</a>
                                    </h1>
                                    <!-- Contact -->
                                    <div class="contact">
                                        <p>
                                            <a href="mailto:info@themevessel.com"><i class="fa fa-envelope-o"></i>ayla_imami@yahoo.co.id</a>
                                        </p>
                                        <p>
                                            <a href="tel:+554XX-634-7071"><i class="fa fa-phone"></i>0813 2952 1538</a>
                                        </p>
                                    </div>
                                    <!-- Sociallist -->
                                    <!-- <ul class="social-list clearfix">
                                        <li>
                                            <a href="#" class="facebook">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="twitter">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="linkedin">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="google">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="rss">
                                                <i class="fa fa-rss"></i>
                                            </a>
                                        </li>
                                    </ul> -->
                                </div>
                            </div>
                            <!-- Agent bos end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Agent section end -->

<!-- Testimonial section start-->

<!-- Testimonial section end -->
<div class="clearfix"></div>

<!-- Counters strat -->

<!-- Counters end -->

<!-- Partners block start -->
<div class="partners-block">
    <div class="container">
        <h3 align="center">Bank Pendukung KPR</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="carousel">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="#">
                                    <img src="<?php echo base_url(); ?>assets/img/brand/bri.png" alt="Bank BRI">
                                </a>
                            </div>
                        </div>
                        <div class="item active">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="#">
                                    <img src="<?php echo base_url(); ?>assets/img/brand/btn.png" alt="Bank BTN">
                                </a>
                            </div>
                        </div>
                        <div class="item active">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="#">
                                    <img src="<?php echo base_url(); ?>assets/img/brand/maybank.png" alt="Bank MayBank">
                                </a>
                            </div>
                        </div>
                        <div class="item active">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="#">
                                    <img src="<?php echo base_url(); ?>assets/img/brand/panin.png" alt="Bank Panin">
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- <a class="left carousel-control" href="#ourPartners" data-slide="prev"><i class="fa fa-chevron-left icon-prev"></i></a> -->
                    <!-- <a class="right carousel-control" href="#ourPartners" data-slide="next"><i class="fa fa-chevron-right icon-next"></i></a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Partners block end -->

<!-- Footer start -->
<footer class="main-footer clearfix">
    <div class="container">
        <!-- Footer top -->
        <div class="footer-top">
            <div class="row">
                <div class="col-lg-5 col-md-4 col-sm-3 col-xs-12">
                    <div class="logo-2">
                        <a href="index.html">
                            <img src="<?php echo base_url(); ?>assets/img/logos/footer-logo.png" alt="footer-logo">
                        </a>
                    </div>
                </div>
                <div class="col-lg-4  col-md-4 col-sm-5 col-xs-12">
                    <form action="#" method="GET">
                        <input type="text" class="form-contact" name="email" placeholder="Enter your email">
                        <button type="submit" name="submitNewsletter" class="btn btn-default button-small">
                            <i class="fa fa-paper-plane"></i>
                        </button>
                    </form>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <ul class="social-list clearfix">
                        <!-- <li><a href="#"><i class="fa fa-rss"></i></a></li> -->
                        <!-- <li><a href="#"><i class="fa fa-google-plus"></i></a></li> -->
                        <li><a href="https://www.instagram.com/dutaindahresidence/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://twitter.com/DutaIndahR88" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.facebook.com/indah.lesmana.73997" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Footer info-->
        <div class="footer-info">
            <div class="row">
                <!-- About us -->
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="footer-item">
                        <div class="main-title-2">
                            <h1>Contact Us</h1>
                        </div>
                        <p>
                            Memiliki rumah dan rukan di Duta Indah Residence adalah pilihan tepat. Duta Indah Residence memiliki lahan terbesar di banding perumahan lain dikelasnya. Bebas banjir, Aman, Nyaman, dll.
                        </p>
                        <ul class="personal-info">
                            <li>
                                <i class="fa fa-map-marker"></i>
                                Alamat: Jl. Prabu Kiansantang No. 88 Sangiang Tangerang
                            </li>
                            <li>
                                <i class="fa fa-envelope"></i>
                                Email:<a href="mailto:grandduta123@gmail.com"> grandduta123@gmail.com</a>
                            </li>
                            <li>
                                <i class="fa fa-phone"></i>
                                Phone: <a href="#">(021) 557.30.888</a>
                            </li><!-- 
                            <li>
                                <i class="fa fa-fax"></i>
                                Fax: +55 4XX-634-7071
                            </li> -->
                        </ul>
                    </div>
                </div>
                <!-- Links -->
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <div class="footer-item">
                        <div class="main-title-2">
                            <h1>Links</h1>
                        </div>
                        <ul class="links">
                            <li>
                                <a href="index.html">Beranda</a>
                            </li>
                            <li>
                                <a href="#">Dijual</a>
                            </li>
                            <li>
                                <a href="#">Disewa</a>
                            </li>
                            <li>
                                <a href="#">Properti Baru</a>
                            </li>
                            <li>
                                <a href="#">KPR</a>
                            </li>
                            <li>
                                <a href="#">Hubungi Kami</a>
                            </li>
                            <li>
                                <a href="#">Tentang Kami</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Tags -->
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="footer-item tags-box">
                        <div class="main-title-2">
                            <h1>Tags</h1>
                        </div>
                        <ul class="tags">
                            <li><a href="#">Gambar</a></li>
                            <li><a href="#">KPR</a></li>
                            <li><a href="#">Tangerang</a></li>
                            <li><a href="#">Murah</a></li>
                            <li><a href="#">Rumah</a></li>
                            <li><a href="#">Rukan</a></li>
                            <li><a href="#">Terbaik</a></li>
                            <li><a href="#">Termewah</a></li>
                            <li><a href="#">Dijual</a></li>
                        </ul>
                    </div>
                </div>
                <!-- Recent cars -->
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="footer-item popular-posts">
                        <div class="main-title-2">
                            <h1>popular posts</h1>
                        </div>
                        <div class="media">
                            <div class="media-left">
                                <img class="media-object" src="<?php echo base_url(); ?>assets/img/properties/small-ax60.jpg" alt="small-properties-1">
                            </div>
                            <div class="media-body">
                                <h3 class="media-heading">
                                    <a href="#">Alexandrite 60</a>
                                </h3>
                                <p>01 April 2018</p>
                                <!-- <div class="comments-icon">
                                    <i class="fa fa-comments"></i>45 comments
                                </div> -->
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-left">
                                <img class="media-object" src="<?php echo base_url(); ?>assets/img/properties/small-ax90.jpg" alt="small-properties-2">
                            </div>
                            <div class="media-body">
                                <h3 class="media-heading">
                                    <a href="#">Alexandrite 90</a>
                                </h3>
                                <p>01 April 2018</p>
                                <!-- <div class="comments-icon">
                                    <i class="fa fa-comments"></i>32 comments
                                </div> -->
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-left">
                                <img class="media-object" src="<?php echo base_url(); ?>assets/img/properties/small-rukan.jpg" alt="small-properties-3">
                            </div>
                            <div class="media-body">
                                <h3 class="media-heading">
                                    <a href="#">Rukan Emerald Arcade</a>
                                </h3>
                                <p>01 April 2018</p>
                                <!-- <div class="comments-icon">
                                    <i class="fa fa-comments"></i>58 comments
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer end -->

<!-- Copy right start -->
<div class="copy-right">
    <div class="container">
        &copy;  2018 <a href="#" target="_blank">Grand Duta Residence</a>. Seluruh hak cipta. PT. DUTA INDAH PROPERTINDO. A Member of Duta Indah Group.
    </div>
</div>
<!-- Copy end right-->
<?php
    $this->load->view('layout/footer');
?>
</body>
</html>