<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_log extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->database(); // load database
        $this->load->library('table');
        $this->load->library('pagination');
        $this->load->helper('url');
        $this->load->model('m_userlog');
    }

	public function index()
	{
		$this->load->view('backend/pages/user_log/index');
	}

	public function ajax_list()
    {
        $list = $this->m_userlog->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $m_userlog) {
            $no++;
            $row = array();
            $row[] = $m_userlog->username;
            $row[] = $m_userlog->email;
 
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_m_userlog('."'".$m_userlog->id."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_m_userlog('."'".$m_userlog->id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_userlog->count_all(),
                        "recordsFiltered" => $this->m_userlog->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
 
    public function ajax_edit($id)
    {
        $data = $this->m_userlog->get_by_id($id);
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
        $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
            );
        $insert = $this->m_userlog->save($data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_update()
    {
        $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
            );
        $this->m_userlog->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_delete($id)
    {
        $this->m_userlog->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
}
