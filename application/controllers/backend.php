<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->database(); // load database
        $this->load->library('table');
        $this->load->library('pagination');
        $this->load->helper('url');
        $this->load->model('m_login');
    }

	public function index()
	{
		$this->load->view('backend/login');
	}

	public function aksi_login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $where = array(
            'username' => $username,
            'password' => md5($password)
            );
        $cek = $this->m_login->cek_login("tb_user_login",$where)->num_rows();
        $cek2 = $this->m_login->cek_login("tb_user_login",$where)->result();
        // $this->m_login->get_ip($data);
        if($cek > 0){
            $timer = 0;
            if ($timer != 0) {
                clearInterval($timer);
                $timer = 0;
                // second step: implement the timer again
                $timer = setInterval("logout()", 1);
                // completed the reset of the timer
            }
            $data_session = array(
                'id' => $cek2[0]->id,
                'nama' => $username,
                'status' => "login"
                );

            $this->session->set_userdata($data_session);
            redirect(base_url("dashboard"));
        }else{
            echo "<script>
                    alert('Username dan Password Salah !!!');
                    window.location.replace('../backend');
                </script>";
        }
    }

    public function logout(){
        $data = array(
            'username' => $this->session->userdata("nama"),
            'last_logout' => $this->time = date('Y-m-d H:i:s')
        );
        // $this->m_login->get_log($data);
        $this->load->driver('cache');
        $this->session->sess_destroy();
        $this->cache->clean();
        ob_clean();
        redirect(base_url('backend'));
    }
}
